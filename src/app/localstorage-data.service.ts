import { Injectable } from '@angular/core';
// import { get } from 'http';
import { studentForm } from './interface';

@Injectable({
  providedIn: 'root'
})
export class LocalstorageDataService {

  constructor() { }

  setData(receiveData: studentForm) {
    debugger
    // let getData=this.getdata();
    // getData.push(receiveData);
    // localStorage.setItem("myObj_string",JSON.stringify(getData));
    let datas = receiveData

    let apps_data: studentForm[] = JSON.parse(localStorage.getItem("myObj_string") as string) || [];
    apps_data.push(datas);

    var serial_data = JSON.stringify(apps_data)
    localStorage.setItem("myObj_string", serial_data);
  }

  getdata() {

    return JSON.parse(localStorage.getItem("myObj_string") as string) || [];

    // let serialised_data = []

    // let get_myvalue: any = localStorage.getItem("myObj_string");

    // if (get_myvalue) {
    //   serialised_data = JSON.parse(get_myvalue)
    // }
    // return serialised_data;

  }

  updateData(receiveData: any) {


    let tableList = this.getdata();

    for (var i = 0; i < tableList.length; i++) {

      if (tableList[i].rollNO == receiveData.rollNO) {
        tableList[i] = receiveData;
        alert("Updated Successfully")
      }
    }
    let table = JSON.stringify(tableList);
    localStorage.setItem('myObj_string', table);

  }

  deleteSelectedRow(index: number) {
    debugger

    let tableList = this.getdata();

    tableList.splice(index, 1);

    var datass = JSON.stringify(tableList);
    localStorage.setItem("myObj_string", datass);

  }
}
