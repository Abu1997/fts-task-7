
export interface studentForm {
    rollNO: number;
    studentName: string;
    fatherName: string;
    gender: string;
    birthDate: string;
    depart: string;
    address: string;
    email: string;
    password: string;
    confirmPassword:string;
}
