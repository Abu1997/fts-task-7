import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { studentForm } from './interface';
import { LocalstorageDataService } from './localstorage-data.service';
declare let $: any;



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'TASK-CRUD';

  get studentName(){
    return this.studentData.get('studentName')
  }

  constructor(private fb: FormBuilder, private service: LocalstorageDataService) { }

  ngOnInit() {
    this.retrieveData();
  }

  studentData = this.fb.group({
    rollNO: ['', Validators.required],
    studentName: ['', [Validators.required, Validators.minLength(3)]],
    fatherName: ['', [Validators.required, Validators.minLength(3)]],
    gender: ['', Validators.required],
    birthDate: ['', Validators.required],
    depart: ['', Validators.required],
    address: ['', Validators.required],
    email: ['', Validators.required],
    password: ['', Validators.required],
    confirmPassword:['',Validators.required]
  })

  studentList: studentForm[] = [];

  isEdit = false;

  createData() {
    this.studentData.patchValue({
      rollNO: '',
      studentName: '',
      fatherName: '',
      gender: '',
      birthDate: '',
      depart: '',
      address: '',
      email: '',
      password: '',
      confirmPassword:''
    })
    this.isPopupOpen = true
  }

  submitData() {
    debugger


    if (this.studentData.valid) {

debugger
      let _data: studentForm = this.studentData.value;
      // (this.service.setData(_data))

      // if (!this.isEdit) {
      //   this.service.setData(_data);
      //   alert("Created successfully!");

      // } else {
      //   this.service.updateData(_data);
      // }
      if (!this.isEdit){
        this.service.setData(_data);
        this.studentList = this.service.getdata()
      alert("Created successfully!");
      } else{
        this.service.updateData(_data);
      }
    }
    this.retrieveData();
    this.studentData.reset();

    this.isPopupOpen = false;
  }


  retrieveData() {
    debugger

    this.studentList = this.service.getdata()
  }

  open() {
    this.isPopupOpen = true;
  }

  public isPopupOpen = false;

  closePopup() {
    this.isPopupOpen = false;
  }

  editData(rollNO: any) {
    debugger


    this.isEdit = true;
    this.isPopupOpen = true;


    for (var i = 0; i < this.studentList.length; i++) {

      if (rollNO == this.studentList[i].rollNO) {
debugger
        this.studentData.patchValue({

          rollNO: this.studentList[i].rollNO,
          studentName: this.studentList[i].studentName,
          fatherName: this.studentList[i].fatherName,
          gender: this.studentList[i].gender,
          birthDate: this.studentList[i].birthDate,
          depart: this.studentList[i].depart,
          address: this.studentList[i].address,
          email: this.studentList[i].email,
          password: this.studentList[i].password,
          confirmPassword:this.studentList[i].confirmPassword
        });
      }

    }



  }

  deleteData(index: number) {
    debugger;
    if (confirm("Are you sure want to delete?")) {
      this.service.deleteSelectedRow(index);
      this.retrieveData()
    }
  }
}

